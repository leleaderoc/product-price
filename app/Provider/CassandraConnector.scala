package Provider

import java.net.InetAddress

import com.datastax.driver.core.Cluster
import com.websudos.phantom.connectors.{SessionProvider, KeySpace}
import com.websudos.phantom.dsl._
import play.api.Play._

import scala.collection.JavaConversions._

trait CassandraConnector extends SessionProvider {

  implicit val space: KeySpace = Connector.keyspace

  val cluster = Connector.cluster

  override implicit lazy val session: Session = Connector.session

  def disconnect() = {
    session.close()
    cluster.close()
  }
}

private object Connector {

  // Implicit Forward pipe operation
  implicit class PipedObject[A](value: A) {
    def |>[B](f: A => B): B = f(value)
  }

  val hostsOption = current.configuration.getStringList("cassandra.hosts")
  val keySpaceOption = current.configuration.getString("cassandra.key-space")
  val usernameOption = current.configuration.getString("cassandra.username")
  val passwordOption = current.configuration.getString("cassandra.password")


  val (keyspace, cluster, session) = (hostsOption, keySpaceOption) match {
    case (Some(hosts), Some(keySpaceString)) =>
      val inets = hosts map InetAddress.getByName

      val keyspace: KeySpace = KeySpace(keySpaceString)

      val cluster =
        (Cluster.builder()
          .addContactPoints(inets) |> (
          builder =>
            (usernameOption, passwordOption) match {
              case (Some(username), Some(password)) =>
                builder.withCredentials(username, password)
              case _ => builder
            }
          )).build()

      val session: Session = cluster.connect(keyspace.name)

      (keyspace, cluster, session)
    case _ => throw new Exception("Unable to read cassandra config")
  }
}