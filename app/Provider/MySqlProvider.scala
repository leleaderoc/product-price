package Provider

import anorm.SqlParser._
import anorm._
import models.{Site, Product}
import play.api.Play.current
import play.api.db.DB

object MySqlProvider {

  // Map of sites at MySql, keys are the ids of the sites
  val sites: Map[Int, Site] = DB.withConnection("mysql") {
    implicit connection =>
      SQL("select id, name from site").as(Site.parser *).toMap
  }

  /**
   * Execute fetch multiple products
   *
   * @param page
   * @param pageSize
   * @param orderBy
   * @param filter
   * @return Seq of [[Product]]
   */
  def fetchProducts(page: Int, pageSize: Int, orderBy: Int, filter: String): Seq[Product] = {

    val offset = pageSize * page
    val (orderByVal, direction) = if (orderBy > 0) (orderBy, "ASC") else (orderBy * -1, "DESC")

    // Note that urls from different sites are concatenated to get single column
    DB.withConnection("mysql") { implicit connection =>
      SQL(
        s"""
           |select product.id as product_id, max(product.name) as product_name,
           |max(brand.name) as brand_name, max(category.name) as category_name,
           |GROUP_CONCAT(
           |  DISTINCT CONCAT(site.name,'::',producturl.url)
           |  ORDER BY site.id
           |  SEPARATOR '||'
           |) as urls
           |from product
           |inner join brand on brand.id = product.brand_id
           |inner join category on category.id = product.category_id
           |left join producturl on producturl.product_id = product.id
           |inner join site on site.id = producturl.site_id
           |where product.name like {filter}
           |group by product.id
           |order by {orderBy} $direction
            |limit {pageSize} offset {offset}
        """.stripMargin
      ).on(
          'pageSize -> pageSize,
          'offset -> offset,
          'filter -> filter,
          'orderBy -> orderByVal
        ).as(Product.simple *)
    }
  }

  /**
   * Execute fetch single product
   *
   * @param productId
   * @return Option of [[Product]]
   */
  def fetchProduct(productId: Int): Option[Product] = {

    // Note that urls from different sites are concatenated to get single column
    DB.withConnection("mysql") { implicit connection =>
      val products = SQL(
        s"""
           |select product.id as product_id, max(product.name) as product_name,
           |max(brand.name) as brand_name, max(category.name) as category_name,
           |GROUP_CONCAT(
           |  DISTINCT CONCAT(site.name,'::',producturl.url)
           |  ORDER BY site.id
           |  SEPARATOR '||'
           |) as urls
           |from product
           |inner join brand on brand.id = product.brand_id
           |inner join category on category.id = product.category_id
           |left join producturl on producturl.product_id = product.id
           |inner join site on site.id = producturl.site_id
           |where product.id = {productId}
           |group by product.id
        """.stripMargin
      ).on(
          'productId -> productId
        ).as(Product.simple *)

      // The result can return only one product, no need to check rest
      products match {
        case product :: ps => Some(product)
        case Nil => None
      }
    }
  }

  /**
   * Get the count of products with current filter
   *
   * @param filter
   * @return count of the products
   */
  def totalRows(filter: String): Long = {

    DB.withConnection("mysql") { implicit connection =>
      SQL(
        """
          |select count(1) from product
          |inner join brand on brand.id = product.brand_id
          |inner join category on category.id = product.category_id
          |where product.name like {filter}
        """.stripMargin
      ).on(
          'filter -> filter
        ).as(scalar[Long].single)
    }
  }
}
