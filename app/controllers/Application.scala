package controllers

import models._
import play.api.Play.current
import play.api.i18n.Messages.Implicits._
import play.api.mvc._
import views._

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Manage a database of products
 */
class Application extends Controller {
  
  /**
   * This result directly redirect to the application home.
   */
  val Home = Redirect(routes.Application.list(0, 2, ""))
  
  // -- Actions

  /**
   * Handle default path requests, redirect to product list
   */  
  def index = Action { Home }
  
  /**
   * Display the paginated list of products.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on product names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(html.list(
      Product.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")),
      orderBy, filter
    ))
  }

  /**
   * Display single product with prices asynchronously
   *
   * @param id Product id
   */
  def single(id: Int) = Action.async { implicit request =>
    Product.single(id) map (productOption => Ok(html.single(productOption)))
  }

}
            
