package models

import java.util.Date

import com.datastax.driver.core.Session
import com.websudos.phantom.CassandraTable
import com.websudos.phantom.column.DateColumn
import com.websudos.phantom.dsl._

import scala.collection.JavaConverters._
import scala.concurrent.Future
import scala.util.Try

/**
 * Definition of Cassandra Table
 */
class ProductPrices extends CassandraTable[ConcreteProductPrices, SinglePrice] {

  override val tableName = "ProductPrices"

  object productId extends IntColumn(this) with PartitionKey[Int]
  object siteId extends IntColumn(this) with PrimaryKey[Int] with ClusteringOrder[Int] with Ascending
  object date extends DateColumn(this) with PrimaryKey[Date] with ClusteringOrder[Date] with Ascending
  object price extends BigDecimalColumn(this)

  def fromRow(row: Row): SinglePrice = {
    SinglePrice(
      productId(row),
      siteId(row),
      date(row),
      price(row)
    )
  }
}
/**
 * Abstract child of [[ProductPrices]] for using fetch operations
 */
case class SinglePrice(productId: Int, siteId: Int, date: Date, price: BigDecimal)

abstract class ConcreteProductPrices extends ProductPrices with RootConnector {

  /**
   * Execute select operation for a product
   *
   * @param id Product id
   * @return Future of List of [[SinglePrice]]
   */
  def selectPrices(id: Int)(implicit session: Session): Future[Option[List[SinglePrice]]] = {
    val statement = select
      .where(_ => productId eqs id)
      .statement()
      .setConsistencyLevel(ConsistencyLevel.ALL)

    Future(Try(session.execute(statement).all().asScala.toList map fromRow).toOption)
  }
}