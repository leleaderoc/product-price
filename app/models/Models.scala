package models

import Provider.{CassandraConnector, MySqlProvider}
import anorm.SqlParser._
import anorm._
import play.api.Play.current
import play.api.cache.Cache

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps

case class Product(id: Int, title: String, brand: Brand, category: Category, urlList: List[ProductUrl], prices: List[SitePrice] = Nil)

case class Brand(name: String)

case class Category(name: String)

case class Site(name: String)

case class SitePrice(site: Site, prices: Map[Long, BigDecimal])

case class ProductUrl(site: Site, url: String)


/**
 * Helper for pagination.
 */
case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

object Product extends ConcreteProductPrices with CassandraConnector {


  /**
   * MySql DB parser for product
   */
  val simple = {
    get[Int]("product_id") ~
    get[String]("product_name") ~
    get[String]("brand_name") ~
    get[String]("category_name") ~
    get[String]("urls")  map {
      case id~name~brand_name~category_name~urls=> Product(id, name, Brand(brand_name), Category(category_name), createUrlList(urls))
    }
  }

  /**
   * Return [[Page]] data for products from MySql
   *
   * @param page Current page number (starts from 0)
   * @param pageSize No of products at a page
   * @param orderBy Column to be sorted
   * @param filter Filter applied on product names
   * @return [[Page]] data
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[Product] = {

    val offset = pageSize * page

    val products = MySqlProvider.fetchProducts(page, pageSize, orderBy, filter)

    Cache.set("products", products)

    val totalRows = MySqlProvider.totalRows(filter)

    Page(products, page, offset, totalRows)
  }

  /**
   * Return information of single product with price information
   *
   * @param productId
   * @return Future value of a [[Product]]
   */
  def single(productId: Int): Future[Option[Product]] = {

    val productPricesFuture = selectPrices(productId)

    val productsCache = Cache.getAs[Seq[Product]]("products") getOrElse Nil

    val productFuture = Future(productsCache.find(p => p.id == productId) orElse MySqlProvider.fetchProduct(productId))

    // Futures
    for {
      productOption <- productFuture
      productPricesOption <- productPricesFuture
    } yield {

      // Options
      for {
        product <- productOption
        productPrices <- productPricesOption
      } yield {

        // Lens Copy
        product.copy(prices =
          // collect because avoiding records with unknown site id
          (productPrices.groupBy(_.siteId) collect {
            case (Site(site), prices) =>
              SitePrice(site,
                (prices map (price => price.date.getTime -> price.price)).toMap
              )
          }).toList
        )
      }
    }
  }

  /**
   * Extract Site urls from a concatenated string
   *
   * @param rawData
   * @return List of [[ProductUrl]]
   */
  def createUrlList(rawData: String): List[ProductUrl] = {
    rawData.split("""\|\|""") collect {
      case ProductUrl(productUrl) => productUrl
    } toList
  }

}

/**
 * Extractor for [[ProductUrl]]
 */
object ProductUrl {

  def unapply(siteUrl: String): Option[ProductUrl] = {
    siteUrl.split("::") match {
      case Array(siteName, url) =>
        Some(ProductUrl(Site(siteName), url))
      case _ => None
    }
  }
}

/**
 * Extractor and parser for [[Site]]
 */
object Site {
  val parser =
    get[Int]("id") ~
    get[String]("name") map {
      case id~name => id -> Site(name)
    }

  def unapply(siteId: Int): Option[Site] = MySqlProvider.sites.get(siteId)
}

