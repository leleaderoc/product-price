# README #

Product Price (Web - Play Framework)

### What is this repository for? ###

* This project is created for the challenge organised by Cimri

### How do I get set up? ###

* There should a MySql DB Shema explained at https://bitbucket.org/leleaderoc/parseproduct
* Cassandra and MySql connection information are stored at conf/application.conf
* Scala version: 2.11.7
* Phantom version: 1.16.0
* Cassandra Version: 2.1.2
* Play activator needs to be installed

### Who do I talk to? ###

* Evaluator of the challenge