name := "product-database-scala"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.11.7"

resolvers ++= Seq("scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
                Resolver.sonatypeRepo("releases"),
                Resolver.sonatypeRepo("snapshots"),
                Resolver.bintrayRepo("websudos", "oss-releases")
              )

libraryDependencies ++= {

  val phantomV = "1.16.0"

  Seq(
    "com.websudos"        %%  "phantom-dsl"               % phantomV,
    "com.websudos"        %%  "phantom-testkit"           % phantomV,
    "com.websudos"        %%  "phantom-connectors"        % phantomV,
    "org.scalatest"       %%  "scalatest"                 % "2.2.4"     % "test",
    "com.typesafe"        %   "config"                    % "1.3.0",
    jdbc,
    evolutions,
    specs2 % Test,
    cache,
    "com.typesafe.play" %% "anorm" % "2.4.0",
    "org.webjars" % "jquery" % "2.1.4",
    "org.webjars" % "bootstrap" % "3.3.5",
    "mysql" % "mysql-connector-java" % "5.1.28"
  )
}

lazy val root = (project in file(".")).enablePlugins(PlayScala)

routesGenerator := InjectedRoutesGenerator